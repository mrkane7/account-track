//
//  ViewController.swift
//  Account Track
//
//  Created by Marcus Kane on 2/22/23.
//

import UIKit


struct theUser{
    static var amount = 0.00;
    static var transactions = [trans]();
};

struct trans : Codable{
    var price = 0.00;
    var date = "";
    var time = "";
};

class ViewController: UIViewController {
    
    @IBOutlet weak var AmountLeftLabel: UILabel!
    @IBOutlet weak var AmountTextField: UITextField!
    @IBOutlet weak var spendLabel: UIButton!
    @IBOutlet weak var depositLabel: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    
    let date = NSDate();
    let calendar = NSCalendar.current;
    

    
    /*============================DidLoad=====================================*/
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap);
        
        loadSaveData();
        
        
        
        
        loadTable()
        UISetup();
        
    }
    
    
    /*============================Buttons=====================================*/
    
    @IBAction func amountTextAction(_ sender: Any) {
        
        if (AmountTextField.text != "")
        {
            enableSpendDeposit();
        }
    }
    
    @IBAction func spendBtn(_ sender: Any) {
        var transaction = trans()
        
        disableSpendDeposit();
        let num = Double(AmountTextField.text ?? "0") ?? 0
        theUser.amount -= num;
        updateDisplay();
        AmountTextField.text = "";
        
        transaction.price = Double(String(format: "%.2f", 0 - num)) ?? 0.00;
        transaction.date = pullDate();
        transaction.time = pullTime();
        
        theUser.transactions.append(transaction);
        tableView.reloadData();
        saveData();
    }
    
    @IBAction func depositBtn(_ sender: Any) {
        var transaction = trans()
        
        disableSpendDeposit();
        let num = Double(AmountTextField.text ?? "0") ?? 0
        theUser.amount += num;
        updateDisplay();
        AmountTextField.text = "";
        
        transaction.price = Double(String(format: "%.2f", num)) ?? 0.00;
        transaction.date = pullDate();
        transaction.time = pullTime();
        
        theUser.transactions.append(transaction);
        tableView.reloadData();
        saveData();
    }
    
    /*============================Functions=====================================*/
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func enableSpendDeposit()
    {
        spendLabel.isEnabled = true;
        depositLabel.isEnabled = true;
        
        spendLabel.backgroundColor = #colorLiteral(red: 0, green: 0.9914394021, blue: 1, alpha: 1);
        depositLabel.backgroundColor = #colorLiteral(red: 0, green: 0.9914394021, blue: 1, alpha: 1);
    }
    
    func disableSpendDeposit()
    {
        spendLabel.backgroundColor = .lightGray;
        depositLabel.backgroundColor = .lightGray;
        
        spendLabel.isEnabled = false;
        depositLabel.isEnabled = false;
    }
    
    func UISetup()
    {
        spendLabel.layer.cornerRadius = 15;
        depositLabel.layer.cornerRadius = 15;
        
        spendLabel.backgroundColor = .lightGray;
        depositLabel.backgroundColor = .lightGray;
        
        updateDisplay();
    }
    
    func updateDisplay()
    {
        AmountLeftLabel.text = "$" + String(format: "%.2f", theUser.amount);
    }
    
    func saveData()
    {
        UserDefaults.standard.setValue(theUser.amount, forKey: "userAmount");
        if let data = try? PropertyListEncoder().encode(theUser.transactions) {
            UserDefaults.standard.set(data, forKey: "theUser.transactions")
        }
    }
    
    func loadSaveData()
    {
        theUser.amount = Double(UserDefaults.standard.integer(forKey: "userAmount"));
        
        if let data = UserDefaults.standard.data(forKey: "theUser.transactions") {
            theUser.transactions = try! PropertyListDecoder().decode([trans].self, from: data)
        }

    }
    
    func pullDate() -> String
    {
        let month = calendar.component(.month, from: date as Date)
        let day = calendar.component(.day, from: date as Date)
        let year = calendar.component(.year, from: date as Date)
        
        let dateString = "\(month)/\(day)/\(year)"
        return dateString;
    }

    func pullTime() -> String
    {
        var hour = calendar.component(.hour, from: date as Date);
        let min = calendar.component(.minute, from: date as Date);
        var temp = ""
        
        if min < 10
        {
            temp = "0\(min)";
        }
        else
        {
            temp = String(min);
        }
        
        if hour > 12
        {
            hour -= 12;
            let timeString = "\(hour):\(temp)pm";
            return timeString;
        }
        
        let timeString = "\(hour):\(temp)am";
        return timeString;
    }
    
    func loadTable()
    {
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    
    
    
    
    
    
    
    
    
    
    

}


extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("you tapped me ")
        
        print("tppedhere")
        print("row selected : \(indexPath.row)")
    }
}

extension ViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return theUser.transactions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var rarr = [trans]();
        let space = ["  ", "   ", "     ", "   -   ", "    - -   ", "   - - -   ", "   - - - -   ", "   - - - - -   ", "  - - - - - -  ", "   - - - - - - -   ", "   - - - - - - - -   ","   - - - - - - -   "]
        var negs = "";
        
        rarr = theUser.transactions.reversed();
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let p = rarr[indexPath.row].price;
        let d = rarr[indexPath.row].date;
        let t = rarr[indexPath.row].time;
        
        var s = String(format: "%.2f", p).count;
        s = space.count - s;
        
        if s < 0
        {
            s = 0;
        }
        
        if (p < 0)
        {
            negs = "";
        }
        
        cell.textLabel!.text = "$\(String(format: "%.2f", p))\(negs)\(space[s])\(d)    \(t)";
        
        if p < 0
        {
            cell.textLabel!.textColor = #colorLiteral(red: 0.9985381961, green: 0.5982193351, blue: 0.005648203194, alpha: 1);
        }
        else
        {
            cell.textLabel!.textColor = #colorLiteral(red: 0, green: 0.9914394021, blue: 1, alpha: 1);
        }
        
        return cell
    }
}
